import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Blackjack {
    private static Scanner sc = new Scanner(System.in);
    private final int deckCount;
    private LinkedList<Card> shoe = new LinkedList<Card>();
    private List<Players> players;
    private Players dealer;

    // Strategies
    private boolean showHints;
    private static Map<HandComp, String> stratStorage = new HashMap<HandComp, String>();

    // Counting
    private boolean showCountHints;
    private int gameCount;
    private static Map<HandComp, String> countStorage = new HashMap<HandComp, String>();

    public Blackjack(int decks, int players) {
        this.deckCount = decks;
        createShoe(deckCount);

        // Set the players
        this.dealer = new Players("Dealer", 0, true);
        this.players = new ArrayList<Players>(players);
        for (int i = 0; i < players; i++) {
            this.players.add(new Players("Player " + i, 1000, false));
        }
    }

    private void createShoe(int decks) {
        // Create the shoe
        for (int i = 0; i < decks; i++) {
            shoe.addAll(new Deck(52).getDeck());
        }
        Collections.shuffle(shoe);
        this.gameCount = 0;
    }

    public Card getNextCard() {
        if (shoe.isEmpty()) {
            createShoe(deckCount);
        }

        // Select card and update count
        Card c = shoe.pop();
        gameCount += c.getCount();
        return c;
    }

    public void play() {
        while (!players.isEmpty()) {
            Iterator<Players> playIter = players.iterator();
            while (playIter.hasNext()) {
                Players p = playIter.next();

                // Out of cash
                if (p.getMoney() <= 0) {
                    System.out.println(p.getName() + " has run out of cash and is out.");
                    playIter.remove();
                    continue;
                }

                System.out.println(p.getName() + ", do you still wish to stay at this table? (yes/no)");

                boolean valid = false;
                while (!valid) {
                    String text = sc.next().toLowerCase();
                    if (text.equals("yes")) {
                        valid = true;
                    } else if (text.equals("no")) {
                        playIter.remove();
                        valid = true;
                    }
                }
            }

            // We still need this check here in case the last person left
            if (!players.isEmpty()) {
                playRound();
            }
        }
    }

    public void playRound() {
        // Handle Betting
        for (Players p : players) {
            boolean valid = false;
            while (!valid) {
                System.out.println(p.getName() + ": What do you bet?");
                double input = sc.nextDouble();
                if (input < 0) {
                    System.out.println("Please bet equal to or more than 0 (0 to sit out this round)");
                    continue;
                }

                if (!p.setBet(input)) {
                    continue;
                }

                System.out.println(p.getName() + " has bet: " + input + " leaving " + p.getName() + " with " + p.getMoney());
                valid = true;
            }
        }

        // Deal Hands
        for (Players p : players) {
            if (p.getBet() > 0) {
                p.gainCard(getNextCard(), p.getCurrentHandNumber());
                p.gainCard(getNextCard(), p.getCurrentHandNumber());
            }
        }
        dealer.gainCard(getNextCard(), dealer.getCurrentHandNumber());
        dealer.gainCard(getNextCard(), dealer.getCurrentHandNumber());

        // Show Dealer Hand
        dealer.showHand(false);

        boolean automaticLose = false;

        // Insurance and Peek for Blackjack
        if (dealer.getHand(0).get(0).getName() == 'A') {
            System.out.println("Insurance Open:");

            for (Players p : players) {
                boolean repeatQuestion = true;

                while (repeatQuestion) {
                    System.out.println(p.getName() + ", do you want insurance and if so how much (0 for no insurance)?");

                    int amount = sc.nextInt();

                    if (amount > p.getBet() / 2) {
                        System.out.println("You cannot insure more than half your bet.");
                    } else {
                        p.setInsuranceBet(amount);
                    }
                }
            }

            // Dealer peeks now
            System.out.println("Insurance closed.");
            if (dealer.getHand(0).get(0).getName() == 'T') {
                automaticLose = true;
            }
        } else if (dealer.getHand(0).get(0).getName() == 'T') {
            if (dealer.getHand(0).get(1).getName() == 'A') {
                automaticLose = true;
            }
        }

        if (!automaticLose) {
            // Handle hit, stand, double down
            for (Players p : players) {
                while (!p.getStand() && !p.checkBust()) {
                    p.showHand(false);

                    String s = p.getName() + ": Hit or Stand";
                    if (p.getHand(p.getCurrentHandNumber()).size() == 2) {
                        s += " or Double Down or Surrender";
                    }
                    s += "?";
                    System.out.println(s);

                    if (showHints) {
                        System.out.println("We think " + p.getName() + " should " + printSuggestion(p, dealer).toUpperCase());
                    }
                    if (showCountHints) {
                        System.out.println("Count hints think " + p.getName() + " should: " + getCountStrategy(p, dealer));
                    }

                    // Handle hit, stand, etc
                    String input = sc.next().toLowerCase();
                    if (input.equals("hit")) {
                        p.hit(getNextCard(), p.getCurrentHandNumber());
                    } else if (input.equals("stand")) {
                        p.setStand(true, false);
                    } else if (input.equals("double")) {
                        if (p.getHand(p.getCurrentHandNumber()).size() > 2) {
                            System.out.println(p.getName() + " can only double down before hitting.");
                        }
                        if (p.getBet() > p.getMoney()) {
                            // TODO DOUBLE FOR LESS
                            System.out.println(p.getName() + " does not have enough money to double down.");
                        } else {
                            p.doubleDown(getNextCard(), p.getCurrentHandNumber());
                        }
                    } else if (input.equals("surrender")) {
                        if (p.getHand(p.getCurrentHandNumber()).size() > 2) {
                            System.out.println(p.getName() + " can only surrender before hitting.");
                        } else {
                            p.surrender(p.getCurrentHandNumber());
                        }
                    } else if (input.equals("split")) {
                        if (p.getHand(p.getCurrentHandNumber()).size() > 2) {
                            System.out.println(p.getName() + " can only split with 2 cards.");
                        } else if (p.getHand(p.getCurrentHandNumber()).get(0).getValue() != p.getHand(p.getCurrentHandNumber()).get(1).getValue()) {
                            System.out.println(p.getName() + " can only split 2 cards of equal value");
                        } else {
                            p.split(p.getCurrentHandNumber());
                        }
                    } else {
                        System.out.println("Invalid Input " + input);
                    }

                    // This is needed in case someone types extra
                    sc.nextLine();
                }

                // If you bust you lose independent of the dealer
                if (p.checkBust()) {
                    p.lose(0);
                }
            }

            // Handle Dealer Draws
            while (dealer.getHandValue() < 17) {
                System.out.println("Dealer draws: ");
                dealer.hit(getNextCard(), dealer.getCurrentHandNumber());
                dealer.showHand(true);
            }
            System.out.println("Dealer stands. Final hand:");
            dealer.showHand(true);
            System.out.println();
        } else {
            System.out.println("Dealer Blackjack. Final hand:");
            dealer.showHand(true);
            System.out.println();
        }

        // Handle Bets
        int dealerScore = dealer.getHandValue();
        for (Players p : players) {
            if (p.checkBust() || p.isSurrender()) {
                continue;
            }

            // This also takes care of dealer blackjack and player blackjack push (since both will be 21 -- although naturals should be 21.5)
            if (dealerScore == p.getHandValue()) {
                p.push();
            } else if (dealerScore > p.getHandValue()) {
                p.lose(dealerScore);
            } else {
                p.win(dealerScore);
            }

            // Insurance bets
            if (p.getInsuranceBet() > 0) { // Precondition: Only set if A is showing, is this safe?
                if (automaticLose) {
                    p.win(dealerScore);
                }
            }

            System.out.println();
        }
        dealer.getHand(dealer.getCurrentHandNumber()).clear();
    }

    // Rest of this is for practicing Blackjack vs casinos

    public void setHints(boolean b) {
        this.showHints = b;
    }

    private String printSuggestion(Players p, Players dealer) {
        List<Card> playerHand = p.getHand(p.getCurrentHandNumber());
        int faceVal = dealer.getHand(p.getCurrentHandNumber()).get(0).getValue();

        // Too lazy for now to make cache handle these
        if (playerHand.size() == 2) {
            char card0 = playerHand.get(0).getName();
            char card1 = playerHand.get(1).getName();
            if (card0 == 'A' || card1 == 'A' || card0 == card1) {
                return getMoveSuggestion(p, dealer);
            }
        }

        int playerVal = p.getHandValue();
        HandComp toCheck = new HandComp(playerVal, faceVal);
        if (stratStorage.containsKey(toCheck)) {
            return stratStorage.get(toCheck);
        }

        // Will need to be changed for split
        String ret = getMoveSuggestion(p, dealer);
        stratStorage.put(toCheck, ret);
        return ret;

    }

    private static String getMoveSuggestion(Players p, Players dealer) {
        // Can probably be easier in a database but if statements for now
        List<Card> playerHand = p.getHand(p.getCurrentHandNumber());
        int faceVal = dealer.getHand(p.getCurrentHandNumber()).get(0).getValue();

        if (playerHand.size() == 2) {
            char card0 = playerHand.get(0).getName();
            char card1 = playerHand.get(1).getName();

            // Handle Aces
            if (card0 == 'A' || card1 == 'A') {
                // Handle Aces
                Card otherC = playerHand.get((card0 == 'A') ? 1 : 0);

                if (otherC.getValue() == 11) {
                    return "Split";
                } else if (otherC.getValue() > 8) {
                    return "Stand";
                } else if (otherC.getValue() == 7) {
                    if (faceVal == 2 || faceVal == 7 || faceVal == 8) {
                        return "Stand";
                    } else if (faceVal < 7) {
                        return "Double Down";
                    } else {
                        return "Hit";
                    }
                } else if (otherC.getValue() == 6) {
                    if (faceVal >= 3 && faceVal <= 6) {
                        return "Double Down";
                    } else {
                        return "Hit";
                    }
                } else if (otherC.getValue() == 5 || otherC.getValue() == 4) {
                    if (faceVal >= 4 && faceVal <= 6) {
                        return "Double Down";
                    } else {
                        return "Hit";
                    }
                } else if (otherC.getValue() == 3 || otherC.getValue() == 2) {
                    if (faceVal == 5 || faceVal == 6) {
                        return "Double Down";
                    } else {
                        return "Hit";
                    }
                } else {
                    return "Hit";
                }
            }

            // Handle Splits
            if (card0 == card1) {
                if (card0 == 'T') {
                    return "Stand";
                } else if (card0 == '9') {
                    if (faceVal == 7 || faceVal >= 10) {
                        return "Stand";
                    } else {
                        return "Split";
                    }
                } else if (card0 == '7') {
                    return (faceVal >= 8 ? "Hit" : "Split");
                } else if (card0 == '6') {
                    return (faceVal >= 7 || faceVal == 2 ? "Hit" : "Split");
                } else if (card0 == '5') {
                    return (faceVal >= 10 ? "Hit" : "Double Down");
                } else if (card0 == '4') {
                    return "Hit";
                } else { // 3-3, 2-2
                    return (faceVal >= 4 && faceVal <= 7 ? "Split" : "Hit");
                }
            }
        }

        // Still go here if the top cases aren't handled
        int value = p.getHandValue();
        if (value >= 17) {
            if (value == 17 && faceVal == 11) {
                return "Surrender"; // This is for TC < -1 for H17, but also basic strategy
            }
            return "Stand";
        } else if (value >= 13 && value <= 16) {
            if (faceVal <= 6) {
                return "Stand";
            } else if (value == 15 && faceVal == 10) {
                return "Surrender";
            } else if (value == 16 && faceVal >= 9) {
                return "Surrender";
            } else {
                return "Hit";
            }
        } else if (value == 12) {
            return (faceVal >= 4 && faceVal <= 6 ? "Stand" : "Hit");
        } else if (value == 11) {
            return (faceVal == 11 ? "Hit" : "Double Down");
        } else if (value == 10) {
            return (faceVal >= 10 ? "Hit" : "Double Down");
        } else if (value == 9) {
            return (faceVal >= 3 && faceVal <= 6 ? "Double Down" : "Hit");
        } else { // value is <= 8
            return "Hit";
        }
    }

    public void setCountHints(boolean b) {
        this.showCountHints = true;
    }

    public String getCountStrategy(Players playerObj, Players dealerObj) {
        List<Card> hand = playerObj.getHand(playerObj.getCurrentHandNumber());
        int player = playerObj.getHandValue();
        int dealer = dealerObj.getHand(dealerObj.getCurrentHandNumber()).get(0).getValue(); // show face up value

        int trueCount = gameCount / (shoe.size() / 52);

        // Fab 4 surrender
        if (player == 14 && dealer == 10 && trueCount > 3) {
            return "Surrender";
        }
        if (player == 15) {
            if (dealer == 11 && trueCount > 1) {
                return "Surrender";
            }
            if (dealer == 10 && trueCount > 0) {
                return "Surrender";
            }
            if (dealer == 9 && trueCount > 2) {
                return "Surrender";
            }
        }

        // Illustrious 18
        if (trueCount > 3) {
            return "Take Insurance";
        }
        if (player == 16 && dealer == 10 && trueCount < 0) {
            return "Hit";
        } else if (player == 15 && dealer == 10 && trueCount < 4) {
            return "Hit";
        } else if (hand.get(0).getValue() == 10 && hand.get(1).getValue() == 10) {
            // ? doesn't make sense =/ maybe this?
            if (dealer == 5 && trueCount < 5) {
                return "Hit for both";
            }
            if (dealer == 6 && trueCount < 4) {
                return "Hit for both";
            }
        } else if (player == 10 && dealer == 10 && trueCount < 4) {
            return "Hit";
        } else if (player == 12 && dealer == 3 && trueCount < 2) { // Index is 1 for H17
            return "Hit";
        } else if (player == 12 && dealer == 2 && trueCount < 3) {
            return "Hit";
        } else if (player == 11 && dealer == 11 && trueCount < 1) { // Index is -1 for H17
            return "Hit";
        } else if (player == 9 && dealer == 2 && trueCount < 1) {
            return "Hit";
        } else if (player == 10 && dealer == 11 && trueCount < 1) {
            return "Hit";
        } else if (player == 9 && dealer == 7 && trueCount < 3) {
            return "Hit";
        } else if (player == 16 && dealer == 9 && trueCount < 5) {
            return "Hit";
        } else if (player == 13 && dealer == 2 && trueCount < -1) {
            return "Hit";
        } else if (player == 12 && dealer == 4 && trueCount == 0) {
            return "Hit";
        } else if (player == 12 && dealer == 5 && trueCount < -2) {
            return "Hit";
        } else if (player == 12 && dealer == 6 && trueCount < -1) {
            return "Hit";
        } else if (player == 13 && dealer == 3 && trueCount < -2) {
            return "Hit";
        }

        // Default
        return "Play by standard strategies";
    }

    public String handleCounting(Players playerObj, Players dealerObj) {
        int player = playerObj.getHandValue();
        int dealer = dealerObj.getHand(dealerObj.getCurrentHandNumber()).get(0).getValue(); // show face up value

        int trueCount = gameCount / (shoe.size() / 52);
        System.out.println("True Count: " + trueCount);

        HandComp toCheck = new HandComp(player, dealer);
        if (countStorage.containsKey(toCheck)) {
            return countStorage.get(toCheck);
        }

        // Will need to be changed for split
        String ret = getCountStrategy(playerObj, dealerObj);
        countStorage.put(toCheck, ret);
        return ret;
    }

    private static class HandComp {
        private final int playerValue;
        private final int dealerCard;

        private HandComp(int p, int d) {
            this.playerValue = p;
            this.dealerCard = d;
        }

        public boolean equals(Object other) {
            if (other.getClass() != this.getClass()) {
                return false;
            }
            HandComp obj = (HandComp) other;
            return obj.playerValue == playerValue && obj.dealerCard == dealerCard;
        }
    }

    public static void main(String[] args) {
        Blackjack bj = new Blackjack(6, 1);
        bj.setHints(true);
        bj.setCountHints(true);
        bj.play();
    }
}
