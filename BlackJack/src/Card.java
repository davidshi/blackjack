public class Card {
	private final char name;
	private final char suit;
	private final int value;
	private final int count;

	public Card(char suit, char name, int value) {
		this.suit = suit;
		this.name = name;
		this.value = value;
		if (value <= 6) {
			this.count = 1;
		} else if (value <= 9) {
			this.count = 0;
		} else {
			this.count = -1;
		}
	}

	public char getName() {
		return name;
	}
	
	public char getSuit() {
		return suit;
	}

	public int getValue() {
		return value;
	}

	public int getCount() {
		return count;
	}
	
	public String getCardName() {
		return String.valueOf(name) + suit;
	}
}
